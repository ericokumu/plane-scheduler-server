import React from "react";

function PassengerDetails({passengerdetails}){
    const {customer_name,c_flight,c_destination}=passengerdetails;
return(

    <div className="passenger">
    <p>{customer_name}</p>
    <p>{c_flight}</p>
    <p>{c_destination}</p>
    </div>
)
}

export default PassengerDetails;