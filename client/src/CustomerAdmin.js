import React from "react";
import CustomerForm from "./CustomerForm";
import CustomerTable from "./CustomerTable";
import axios from "axios";


class CustomerAdmin extends React.Component{
    constructor(props){
        super(props);
        this.state={
            name:"",
            destination:"",
            flight:"",
            planeid:"",
            editing:false,
            formSubmitting:false,
            validationErrors:{},
            formSuccess:false,
            formError:false,
            customerData:[],
            tableLoading:false,
            tableError:false,
            deleteSuccess:false
        }
        this.handleEditCustomer=this.handleEditCustomer.bind(this);
        this.resetFormState=this.resetFormState.bind(this);
        this.handleNameChange=this.handleNameChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleDeleteCustomer=this.handleDeleteCustomer.bind(this);
        
        
    }

    componentDidMount(){
        this.fetchCustomerData();
    }


    fetchCustomerData(){
        this.setState({tableLoading:true,tableError:false});
        axios.get("/api/admin/customer")
        .then(response=>{
            this.setState({
                customerData:response.data,
                tableLoading:false,
                tableError:false
            });
        })
        .catch(error=>{
            this.setState({
                customerData:[],
                tableLoading:false,
                tableError:true
            });
        });
    }

    handleEditCustomer(customer){
        const{Customer_name,planeid,c_destination,c_flight}=customer;
        return ()=>{
            this.setState({
                name:Customer_name,
                destination:c_destination,
                flight:c_flight,
                planeid:planeid,
                editing:true,
                
            });
        }

    }

    handleDeleteCustomer(customer,customerData){
        
        return ()=>{
            const{Customer_name,customerid}=customer;


            if(prompt(`Are you sure you want to delete '${Customer_name}'?`)){
                axios.delete(`/api/admin/customer/${customerid}`)
                .then(response=>{
                    const index=customerData.findIndex(c=>c.customerid===customerid);
                    this.setState({
                        customerData:[
                            ...customerData.slice(0,index),
                            ...customerData.slice(index+1)
                        ],
                        deleteSuccess:true,
                        tableError:false
                    })
                })
                .catch(error=>{
                    this.setState({
                        deleteSuccess:false,
                        tableError:true
                    })
                })

            }

        }

    }

//Form SEction

resetFormState(){
    this.setState({
        name:"",
        destination:"",
        flight:"",
        planeid:"",
        editing:false,
        deleteSuccess:false

    })
}
handleNameChange(e){
    e.preventDefault();
    let name=e.target.name
    this.setState({
        
        [name]:e.target.value
        
    })

}


   //form input validation
   isValid(){
       const{validationErrors,isValid}=this.validateFormInputs(this.state);

       if(!isValid){
           this.setState({validationErrors});
       }
       return isValid;
   }
   validateFormInputs(data){
       const validationErrors={};
       const{name,destination,flight,planeid}=data;

       if(!name||!destination||!flight||!planeid){
           validationErrors.name="This field is required";
       }
       return{
           validationErrors,isValid:Object.keys(validationErrors).length ===0
       };
   }

   //form submition

   handleSubmit(e){
       e.preventDefault();
       const{editing,customerData,id}=this.state;
     console.log([customerData]);

       if(this.isValid()){
           this.setState({
               validationErrors:{},
               formSubmitting:true,
               formSuccess:false,
               formError:false
           });
           if(editing){
              
                let{customerid}=customerData;
                // let val=Customer_name;
                // let vali=customerid;
                
               
               axios.put(`/api/admin/customer/${id}`,{customerid})
               .then(response=>{
                   this.resetFormState();
                   const index=customerData.findIndex(c=>c.customerid===customerid);
                   //const index = customerData.findIndex(cus=>cus.customerid === id);
                   
                   this.setState({
                       formSuccess:true,
                       customerData:[
                           ...customerData.slice(0,index),
                           {id,customerid},
                           ...customerData.slice(index+1)
                       ]
                   });
               }).catch(error=>{
                   this.setState({
                       validationErrors:{},
                       formSubmitting:false,
                       formSuccess:false,
                       formError:true
                   })
               })
           } else{
            const{customerid}=customerData;
               axios.post("/api/admin/customer",{customerid})
               .then(response=>{
                   this.resetFormState();
                   this.setState({
                       formSuccess:true,
                       customerData:[...customerData,{customeri:response.data,customerid}]
                   });
               })
               .catch(error=>{
                   this.setState({
                       validationErrors:{},
                       formSubmitting:false,
                       formSuccess:false,
                       formError:true
                    })
               })
           }
       }

   }

    

render(){
    const{
        name,
        destination,
        flight,
        planeid,
        customerData,
        validationErrors,
        formSubmitting,
        formSuccess,
        formError,
        tableLoading,
        tableError,
        deleteSuccess
    }=this.state;
    
    return(
        <div>
            
        <CustomerForm 
        name={name}
        destination={destination}
        flight={flight}
        planeid={planeid}
        validationErrors={validationErrors}
        formSubmitting={formSubmitting}
        formSuccess={formSuccess}
        formError={formError}
        onResetFormState={this.resetFormState}
        handleNameChange={this.handleNameChange}
        handleSubmit={this.handleSubmit}
        />
        
        <CustomerTable
        customerData={customerData}
        tableLoading={tableLoading}
        tableError={tableError}
        deleteSuccess={deleteSuccess}
        onEditCustomer={this.handleEditCustomer}
        onDeleteCustomer={this.handleDeleteCustomer}
        />
        </div>
    )
}
}

export default CustomerAdmin;