import React from "react";
import "./App.css";


function CustomerForm({
    name,
    destination,
    planeid,
    flight,
    validationErrors,
    formSubmitting,
    formSuccess,
    formError,
    onResetFormState,
    handleNameChange,
    handleSubmit
}){
    const disabled=!name||!destination||!planeid||!flight;

    return(
        <div className="ps-customer-form">
            <h1 className="adm-head">CUSTOMER</h1>
        <form className="ps-form">
        {formSuccess && (
            <p className="ps-alert ps-alert-success">Form submitted successfully</p>
        )}
        {formError && (
            <p className="ps-alert ps-alert-error">Sorry,error submitting form. please retry.</p>
        )}
        <div className="ps-form-row">
        <div className="ps-form-col">
         <label htmlFor="name">Name</label>
         <input 
         type="text" 
         name="name" 
         value={name}
         onChange={handleNameChange}
         />
         <label htmlFor="destination">Destination</label>
         <input 
         type="text" 
         name="destination" 
         value={destination} 
         onChange={handleNameChange}
         />
         <label htmlFor="flight">Flight</label>
         <input 
         type="text" 
         name="flight" 
         value={flight}
         onChange={handleNameChange}
         />
         <label htmlFor="planeId">Plane Id</label>
         <input 
         type="text" 
         name="Planeid" 
         value={planeid}
       onChange={handleNameChange}
         />
          {validationErrors.name && (<span className="ps-form-input-error">
              {validationErrors.name}
          </span>)

          }
        </div>
        </div>
        <button 
        type="submit" 
        onClick={handleSubmit}
        disabled={disabled||formSubmitting}>ADD</button>
        <button 
        type="reset"
        onClick={onResetFormState}
        disabled={formSubmitting}
        
        >Reset</button>

        </form>
        </div>
    )


}

export default CustomerForm;