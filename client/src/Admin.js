import React from "react";
import CustomerAdmin from "./CustomerAdmin";
import PlaneAdmin from "./PlaneAdmin";

function Admin(){

    return(
        <div>
            
            <CustomerAdmin/>
            <PlaneAdmin/>
        </div>
    )

}

export default Admin;