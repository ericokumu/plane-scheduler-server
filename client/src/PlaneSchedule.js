import React from "react";

function PlaneSchedule({ plane }) {
    const { destination, terminal, gate, airline_img,statis, scheduled_time } = plane;

    return (
        <div className="planeschedule">
            <img src={airline_img} alt="airline " className="plane-image" />

            <div className="ps-single-plane">

                {/* <p>{}</p> */}
                <p>{destination}</p>
                <p>{scheduled_time}</p>
                <p>{terminal}</p>
                <p>{gate}</p>
                <p>{statis}</p>

            </div>

        </div>
    );

}

export default PlaneSchedule;