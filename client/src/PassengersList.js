import React from "react";
import axios from "axios";
import PassengerDetails from "./PassengerDetails";
import "./App.css";
import Loading from "./Loading";
import Error from "./Error";

class PassengersList extends React.Component {
    constructor(props){
        super(props);

        this.state={
            passenger:[],
            loading:false,
            error:false
        }
    }

    componentDidMount(){
        this.fetchPassengerDetails();
    }

    fetchPassengerDetails(){
        this.setState({loading:true,error:false});

        axios.get("/api/passengers")
        .then(resp=>{
            this.setState({
                passenger:resp.data,
                loading:false,
                error:false
            })
        })
        .catch(error=>{
            this.setState({
                passenger:[],
                loading:false,
                error:true
            })
        });

    }

    render(){
const{passenger,loading,error}=this.state;


if(loading){
    return <Loading/>
}

if(error){
    return <Error/>
}
return(
    <div >
    <div className="header">

            <h1 className="head-top">Passenger Name</h1>
            <h1 className="head-top">Flight</h1>
            <h1 className="head-top">Destination</h1>
    
    </div>
        
           
        
        
            {passenger.map(item => (
               <PassengerDetails passengerdetails={item}/>
                    
            ))}
           
        
    </div>
)
    }

}

export default PassengersList;