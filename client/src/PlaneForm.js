import React from "react";

function PlaneForm({
    airline,
    destination,
    terminal,
    status,
    flight,
    scheduledtime,
    gate,
    formSubmitting,
    validationErrors,
    handleNameChange,
    resetFormState,
    handleSubmit
}){


    return(
        <div className="ps-planeform-div">
        <div className="ps-planeform-main-body">
        <h1 className="adm-head">PLANE</h1>
            <form className="ps-form">
            <label htmlFor="airline">Airline</label>
            <input type="text"
             name="airline"
             value={airline}
             onChange={handleNameChange}
             />
            <label htmlFor="destination">Destination</label>
            <input type="text" 
            name="destination"
            value={destination}
            onChange={handleNameChange}
            />
            <label htmlFor="terminal">Terminal</label>
            <input type="text" 
            name="terminal"
            value={terminal}
            onChange={handleNameChange}
            />
            <label htmlFor="status">Status</label>
            <input type="text"
             name="status"
             value={status}
             onChange={handleNameChange}
             />
            <label htmlFor="flight">Flight</label>
            <input type="text"
             name="flight"
             value={flight}
             onChange={handleNameChange}
             />
            <label htmlFor="scheduledtime">Scheduled Time</label>
            <input type="Time" 
            name="scheduledtime"
            value={scheduledtime}
            onChange={handleNameChange}
            />
            <label htmlFor="gate">Gate
            <input type="text" 
            name="gate"
            value={gate}
            onChange={handleNameChange}
            /></label>
             {validationErrors.name && (<span className="ps-form-input-error">
              {validationErrors.name}
          </span>)}
            <button type="submit" onClick={handleSubmit} disabled={disabled||formSubmitting}>ADD</button>
            <button type="reset" 
            onClick={resetFormState}
            disabled={formSubmitting}
            >Reset</button>

            </form>
            
            </div>
        </div>
    )
}

export default PlaneForm;