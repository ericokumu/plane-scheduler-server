import React from "react";
import {Router} from "@reach/router";
import NavBar from "./NavBar";
import PlaneSchedulerList from "./PlaneSchedulerList";
import PassengersList from "./PassengersList";
import Admin from "./Admin";
import "./App.css";

// const PLANE = [
//     {
//       "airline_img": "https://d24ndt2yiijez0.cloudfront.net/uploads/carrier/logo/106/Kenya-KQ-logo-new.jpg",
//       "destination": "ENTEBBE",
//       "scheduled_time": "00:11:30",
//       "terminal": "T1A",
//       "gate": "21B",
//       "statis": "Departed"
//     },
//     {
//       "airline_img": "https://s3.india.com/wp-content/uploads/2015/03/emirates-logo.jpg",
//       "destination": "DUBAI",
//       "scheduled_time": "00:15:40",
//       "terminal": "T1B",
//       "gate": "19",
//       "statis": "Scheduled"
//     },
//     {
//       "airline_img": "https://content.presspage.com/uploads/1724/1920_airarabia-logo-bilingual-vertical-negative-666415.jpg?10000",
//       "destination": "SHARJAH",
//       "scheduled_time": "00:13:45",
//       "terminal": "T1C",
//       "gate": "13A",
//       "statis": "Gate_open"
//     }
//   ];


function App (){
return(
    <div className="ps-app">
        <header className="ps-header">
            <NavBar/>
        </header>
        <main className="ps-main">
        <Router>

        <PlaneSchedulerList path="/Schedule_view"/>
        <PassengersList path="/passengers"/>
        <Admin path="/admin"/>

        </Router>
           
        </main>
    </div>
);

}

export default App;