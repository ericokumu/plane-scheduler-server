import React from "react";



function CustomerTable({
    customerData,
    tableLoading,
    tableError,
    deleteSuccess,
    onEditCustomer,
    onDeleteCustomer}){

        if(tableLoading){
            return <p className="ps-table-loading">Loading Customer details....</p>;
        }

return(
    <div className="ps-table">
        <p>CustomerTable</p>
        {deleteSuccess &&(
            <p className="ps-alert ps-alert-success">
            Record deleted Successfully
            </p>
        )}
        {tableError && (
            <p className="ps-alert ps-alert-error">
              Sorry, a server error occured. Please retry.
            </p>
        )}
        <table>
            <thead>
                <tr>
                <th>No</th>
                <th>Name</th>
                <th>Destination</th>
                <th>Flight</th>
                <th>Plane Id</th>
                </tr>
            </thead>
            {/*when customerData array is empty */}
            {customerData.length === 0 && (
            <tbody>
                <tr>
                    <td colSpan="8" className="ps-no-data">
                        No data
                    </td>
                </tr>
            </tbody>
            )}

            {/*when customerData array contains objects */}
            {customerData.length > 0 && (
                <tbody>
                    {customerData.map((customer,index)=>{
                        const{Customer_name,planeid,c_destination,c_flight}=customer;
                        
                        //console.log(customer)

                        return(
                            <tr key={index}>
                            <td>{index+1}</td>
                            <td>{Customer_name}</td>
                            <td>{c_destination}</td>
                            <td>{c_flight}</td>
                            <td>{planeid}</td>
                            <td>
                                <span 
                                className="ps-table-link"
                                onClick={onEditCustomer(customer)}

                                
                                >Edit</span>
                                &nbsp;&nbsp;| &nbsp;&nbsp;
                                <span className="ps-table-link"
                                onClick={onDeleteCustomer(customer,customerData)}
                                
                                >Delete</span>
                            </td>

                            </tr>
                        );
                    })}
                </tbody>
            )}

        </table>

        
    </div>
)
}

export default CustomerTable;