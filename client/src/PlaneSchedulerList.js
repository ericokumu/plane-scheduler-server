import React from "react";
import PlaneSchedule from "./PlaneSchedule";
import axios from "axios";
import Loading from "./Loading";
import Error from "./Error"; 

class PlaneSchedulerList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            plane: [],
            loading: false,
            error: false
        };
    }

    componentDidMount() {
        this.fetchPlaneSchedule();
    }
    fetchPlaneSchedule() {

        this.setState({ loading: true, error: false });

        axios.get("/api/schedule_view").then(response => {
            this.setState({
                plane: response.data,
                loading: false,
                error: false
            });
        }).catch(error => {
            this.setState({
                plane: [],
                loading: false,
                error: true
            });
        });

    }


    render() {
        const { plane, loading, error} = this.state;

        if(loading){
           return <Loading/>
        }

        if(error){
            return <Error/>
        }
        return (
            <div className="ps-container">
                <div className="ps-planeschedulelist">
                    {plane.map(item => (

                        <PlaneSchedule plane={item} />
                    ))}

                </div>
            </div>
        );
    }
}

export default PlaneSchedulerList;