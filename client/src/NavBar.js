import React from "react";
import {Link} from "@reach/router";



function NavBar() {
    return (
        <div className="ps-container">
            <nav className="ps-nav">
                <span className="ps-title">PLANE SCHEDULER</span>
                <Link to="/Schedule_view">Schedule View</Link>
                <Link to="/passengers">Passengers</Link>
                <Link to="/admin">Admin</Link>
            </nav>
        </div>
    );
}

export default NavBar;