import React from "react";
import axios from "axios";
import PlaneForm from "./PlaneForm";
import PlaneTable  from "./PlaneTable";


class PlaneAdmin extends React.Component{
    constructor(props){
        super(props);
        this.state={
            planedata:[],
            tableLoading:false,
            tableError:false,
            deleteSuccess:false,
            airline:"",
            destination:"",
            terminal:"",
            status:"",
            flight:"",
            scheduledtime:"",
            gate:"",
            formSubmitting:false,
            validationErrors:{},
            formSuccess:false,
            formError:false,
            editing:false
        }
        this.handleNameChange=this.handleNameChange.bind(this);
        this.resetFormState=this.resetFormState.bind(this);
        this.handleEditPlanes=this.handleEditPlanes.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleDeletePlane=this.handleDeletePlane.bind(this);
    }
    componentDidMount(){
        this.fetchPlaneDetails();
    }

    fetchPlaneDetails(){
        this.setState({tableLoading:true,tableError:false});
        axios.get("/api/admin/planes").then(response=>{
            this.setState({
                planedata:response.data,
                tableLoading:false,
                tableError:false
            });
        }).catch(error=>{
            this.setState({
                planedata:[],
                tableLoading:false,
                tableError:true
            });
        });

    }

    handleNameChange(e){
        e.preventDefault();
        let name =e.target.name
        this.setState({
            [name]:e.target.value
        })
    }
    resetFormState(){
        this.setState({
            airline:"",
            destination:"",
            terminal:"",
            status:"",
            flight:"",
            scheduledtime:"",
            gate:""

        });
    }
    handleEditPlanes(item){
        const {terminal,gate,airline_img,plane,destination,statis,scheduled_time}=item;
        return ()=>{
            this.setState({
                airline:airline_img,
                destination:destination,
                terminal:terminal,
                status:statis,
                flight:plane,
                scheduledtime:scheduled_time,
                gate:gate

            })
        }

    }

    isValid(){
        const{validationErrors,isValid}=this.validateFormInputs(this.state);
 
        if(!isValid){
            this.setState({validationErrors});
        }
        return isValid;
    }
    validateFormInputs(data){
        const validationErrors={};
        const{airline,destination,flight,terminal,status,scheduledtime,gate}=data;
 
        if(!airline||!destination||!flight||!terminal||!status||!scheduledtime||!gate){
            validationErrors.name="This field is required";
        }
        return{
            validationErrors,isValid:Object.keys(validationErrors).length ===0
        };
    }
 
 
    handleSubmit(e){
        e.preventDefault();
        const{editing,planedata,id}=this.state;
      console.log([planedata]);
 
        if(this.isValid()){
            this.setState({
                validationErrors:{},
                formSubmitting:true,
                formSuccess:false,
                formError:false
            });
            if(editing){
               
                 let{plane}=planedata;
                 // let val=plane;
                 // let vali=plane;
                 
                
                axios.put(`/api/admin/${id}/planes`,{plane})
                .then(response=>{
                    this.resetFormState();
                    const index=planedata.findIndex(c=>c.plane===plane);
                    //const index = planedata.findIndex(cus=>cus.plane === id);
                    
                    this.setState({
                        formSuccess:true,
                        planedata:[
                            ...planedata.slice(0,index),
                            {id,plane},
                            ...planedata.slice(index+1)
                        ]
                    });
                }).catch(error=>{
                    this.setState({
                        validationErrors:{},
                        formSubmitting:false,
                        formSuccess:false,
                        formError:true
                    })
                })
            } else{
             const{plane}=planedata;
                axios.post("/api/admin/planes",{plane})
                .then(response=>{
                    this.resetFormState();
                    this.setState({
                        formSuccess:true,
                        planedata:[...planedata,{id:response.data,plane}]
                    });
                })
                .catch(error=>{
                    this.setState({
                        validationErrors:{},
                        formSubmitting:false,
                        formSuccess:false,
                        formError:true
                     })
                })
            }
        }
 
    }
    handleDeletePlane(item,planedata){
        
        return ()=>{
            const{plane,}=item;


            if(prompt(`Are you sure you want to delete '${plane}'?`)){
                axios.delete(`/api/admin/planes/${plane}`)
                .then(response=>{
                    const index=planedata.findIndex(c=>c.plane===plane);
                    this.setState({
                        planedata:[
                            ...planedata.slice(0,index),
                            ...planedata.slice(index+1)
                        ],
                        deleteSuccess:true,
                        tableError:false
                    })
                })
                .catch(error=>{
                    this.setState({
                        deleteSuccess:false,
                        tableError:true
                    })
                })

            }

        }

    }

    render(){
        const{
            planedata,
            tableError,
            tableLoading,
            deleteSuccess,
            airline,
            destination,
            terminal,
            status,
            flight,
            scheduledtime,
            gate,
            formSubmitting,
            validationErrors

        }=this.state;
        return(
            <div className="ps-plane-admin">
                <PlaneForm
                 airline={airline}
                 destination={destination}
                 terminal={terminal}
                 status={status}
                 flight={flight}
                 scheduledtime={scheduledtime}
                 gate={gate}
                 formSubmitting={formSubmitting}
                 handleNameChange={this.handleNameChange}
                 resetFormState={this.resetFormState}
                 handleSubmit={this.handleSubmit}
                 validationErrors={validationErrors}
                 
                 
                
                
                />




                <PlaneTable 
                planedata={planedata} 
                tableError={tableError} 
                tableLoading={tableLoading}
                deleteSuccess={deleteSuccess}
                handleEditPlanes={this.handleEditPlanes}
                handleDeletePlane={this.handleDeletePlane}
                
                
               
                />
            </div>
        );
    }

}

export default PlaneAdmin;