import React from "react";

function PlaneTable({planedata,tableLoading,tableError,deleteSuccess,handleEditPlanes,handleDeletePlane}){
    if(tableLoading){
        return <p className="ps-table-loading">Loading Plane details....</p>;
    }
return(
    <div className="ps-table">
      <p>CustomerTable</p>
        {deleteSuccess &&(
            <p className="ps-alert ps-alert-success">
            Record deleted Successfully
            </p>
        )}
        {tableError && (
            <p className="ps-alert ps-alert-error">
              Sorry, a server error occured. Please retry.
            </p>
        )}
    <div className="tbl-plane">
        <table>
            <thead>
            <tr>
                <th>No</th>
                <th>Terminal</th>
                <th>Gate</th>
                <th>Airline</th>
                <th>Flight</th>
                <th>Destinaion</th>
                <th>Scheduled</th>
                <th>Status</th>
            </tr>
            </thead>
            {planedata.length===0 && (
            <tbody>
            <tr>
             <td colSpan="8" className="ps-no-data">
                 No data
            </td>
                </tr>
            </tbody>
            )}
            {planedata.length>0 && (
                <tbody>
                    {planedata.map((item,index)=>{
                        const {terminal,gate,airline_img,plane,destination,statis,scheduled_time}=item;
                        return(
                            <tr>
                                <td>{index+1}</td>
                                <td>{terminal}</td>
                                <td>{gate}</td>
                                <td>{airline_img}</td>
                                <td>{plane}</td>
                                <td>{destination}</td>
                                <td>{scheduled_time}</td>
                                <td>{statis}</td>
                                <td>
                                <span 
                                className="ps-table-link"
                               onClick={handleEditPlanes(item)}

                                
                                >Edit</span>
                                &nbsp;&nbsp;| &nbsp;&nbsp;
                                <span className="ps-table-link"
                                onClick={handleDeletePlane(item,planedata)}
                                
                                >Delete</span>
                            </td>
                            </tr>
                        )
                    })}
                </tbody>
            )}
        </table>
        </div>
    </div>
)

}

export default PlaneTable;