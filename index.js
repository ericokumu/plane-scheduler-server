require("dotenv").config();
const express = require("express");
const mysql = require("mysql");
const app = express();
const pool = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
});
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//end-point for passengers
//fetching passengers: name,flight,destination
app.get("/api/passengers", (req, res) => {

    pool.query('select c.customer_name,b.c_flight,b.c_destination from customer c JOIN book b ON b.customerid=c.customerid',
        (error, rows) => {
            if (error) {
                return res.status(500).json({ error });
            }
            res.json(rows)
            console.table(rows)

        })
});
//getting specific passenger
app.get("/api/passengers/:id", (req, res) => {

    pool.query('select c.customer_name,b.c_flight,b.c_destination from customer c JOIN book b ON b.customerid=c.customerid WHERE c.customerid=?',
        [req.params.id],
        (error, rows) => {
            if (error) {
                return res.status(500).json({ error });
            }
            res.json(rows)
            console.table(rows)

        })
});


//end-point for scheduled_view
//fetching details from flight
// updated a query: [ALTER TABLE flight ADD COLUMN gate VARCHAR(20) DEFAULT NULL;] [UPDATE flight SET gate="13A" WHERE planeid=3;]
app.get("/api/schedule_view", (req, res) => {
    pool.query("select f.airline_img,destination,scheduled_time,terminal,gate,statis from flight f WHERE gate<>'null'",
        (error, rows) => {
            if (error) {
                return res.status(500).json({ error });
            }
            return res.json(rows);
            console.table(rows);
        });
});

/**************************************************************************************************
 * ************************************************************************************************
 * ************************************ADMIN PAGE**************************************************
 * ************************************************************************************************
 * ************************************************************************************************
 */

//end-point for admin
//
//Display customer details
app.get("/api/admin/customer", (req, res) => {
    pool.query(
        "SELECT c.customerid,c.Customer_name,c.planeid,c_destination,c_flight FROM customer c JOIN book b ON c.customerid=b.customerid",
        (error, rows) => {
            if (error) {
                return res.status(500).json({ error });
            }
            return res.json(rows);

        });

});

//POST REQUESTS

//Adding new user
app.post("/api/admin/customer", (req, res) => {
    const {
        Customer_name,
        planeid,
        c_destination,
        c_flight,
        customerid
    } = req.body;



    if (!Customer_name || !planeid || !c_destination || !c_flight) {
        return res.status(400).json({ error: "Invalid payload" });
    }
    pool.getConnection((error, connection) => {
        if (error) {
            return res.status(500).json({ error });
        }
        connection.beginTransaction(error => {
            if (error) {
                return res.status(500).json({ error });
            }
            connection.query(
                "INSERT INTO customer(Customer_name,planeid) VALUES(?,?)",
                [Customer_name, planeid],
                (error, results) => {
                    if (error) {
                        return connection.rollback(() => {
                            res.status(500).json({ error });
                        });
                    }
                    const insertId = results.insertId;
                    const customerid = insertId;
                    //const bookingDetails = booking.map(book => [insertId, book]);
                    connection.query(
                        "INSERT INTO book(c_destination,c_flight,customerid) VALUES (?,?,?)",
                        [c_destination, c_flight, customerid],
                        (error, results) => {
                            if (error) {
                                return connection.rollback(() => {
                                    res.status(500).json({ error });
                                })
                            }
                            connection.commit(error => {
                                if (error) {
                                    return connection.rollback(() => {
                                        res.status(500).json({ error });
                                    });
                                }
                                connection.release();
                                res.json(insertId);
                            });
                        }
                    );
                }
            );
        });
    })
});

//PUT REQUEST
//Updating customer details

app.put("/api/admin/customer/:id", (req, res) => {
    const {
        Customer_name,
        planeid,
        c_destination,
        c_flight,
        customerId,
        customerid

    } = req.body;

    if (!Customer_name || !planeid || !c_destination || !c_flight) {
        return res.status(400).json({ error: "error invalid payload" });

    }
    pool.getConnection((error, connection) => {
        if (error) {
            return res.status(500).json({ error });
        }
        connection.beginTransaction(error => {
            if (error) {
                return res.status(500).json({ error });
            }
            const customerId = [req.params.id];

            connection.query(
                "UPDATE customer SET Customer_name=?,planeid=? WHERE customerid=?",
                [Customer_name, planeid, customerId],
                (error, results) => {
                    if (error) {
                        return connection.rollback(() => {
                            res.status(500).json({ error });
                        })
                    }


                    connection.query(
                        "DELETE FROM book WHERE customerid=?",
                        [customerId],
                        (error, results) => {
                            if (error) {
                                return connection.rollback(() => {
                                    res.status(500).json({ error });
                                });
                            }

                            connection.query(
                                "INSERT INTO book (c_destination,c_flight,customerid) VALUES (?,?,?)   ",
                                [c_destination, c_flight, customerId],
                                (error, results) => {
                                    if (error) {
                                        return connection.rollback(() => {
                                            res.status(500).json({ error })
                                        });
                                    }

                                    connection.commit(error => {
                                        if (error) {
                                            return connection.rollback(() => {
                                                res.status(500).json({ error });

                                            })
                                        }
                                        connection.release();
                                        res.json(results.affectedRows);
                                    })
                                });

                        })

                })
        })
    });



});

//DELETE REQUEST
//Removing a customer based on the customer id

app.delete("/api/admin/customer/:id", (req, res) => {
    const customerid = [req.params.id];

    pool.getConnection((error, connection) => {
        if (error) {
            return res.status(500).json({ error });
        }
        connection.beginTransaction(error => {
            if (error) {
                return res.status(500).json({ erro });
            }
            pool.query(
                "DELETE FROM book WHERE customerid = ? ",
                [customerid],
                (error, results) => {
                    if (error) {
                        return connection.rollback(() => {
                            res.status(500).json({ error });
                        });
                    }
                    pool.query(
                        "DELETE FROM customer WHERE customerid = ? ",
                        [customerid],
                        (error, results) => {
                            if (error) {
                                return connection.rollback(() => {
                                    res.status(500).json({ error });
                                });
                            }
                            connection.commit(error => {
                                if (error) {
                                    return connection.rollback(() => {
                                        res.status(500).json({ error });
                                    });
                                }
                                connection.release();
                                res.json(results.affectedRows);
                            })
                        });
                });
        });
    });


});

//GET REQUEST -- plane details

app.get("/api/admin/planes", (req, res) => {
    pool.query(
        " SELECT terminal,gate,airline_img,plane,destination,scheduled_time,statis FROM flight",
        (error, rows) => {
            if (error) {
                return res.status(500).json({ error });
            }
            res.json(rows);
        });
});

//POST REQUEST -- plane details


app.post("/api/admin/planes", (req, res) => {
    const {
        terminal,
        gate,
        airline_img,
        plane,
        destination,
        scheduled_time,
        statis
    } = req.body;

    if (!terminal || !gate || !airline_img || !plane || !destination || !scheduled_time || !statis) {
        return res.status(400).json({ error: "Invalid payload" });
    }
    pool.getConnection((error, connection) => {
        if (error) {
            return res.status(500).json({ error });
        }

        connection.beginTransaction(error => {
            if (error) {
                return res.status(500).json({ error });

            }
            connection.query(
                "INSERT INTO flight (terminal,gate,airline_img,plane,destination,scheduled_time,statis) VALUES(?,?,?,?,?,?,?) ",
                [terminal, gate, airline_img, plane, destination, scheduled_time, statis],
                (error, results) => {
                    if (error) {
                        connection.rollback(() => {
                            return res.status(500).json({ error });
                        });
                    }
                    connection.commit(error => {
                        if (error) {
                            connection.rollback(() => {
                                return res.status(500).json({ error });
                            });
                        }
                        connection.release();
                        res.json(results.insertId);

                    });
                });

        });
    });
});

//PUT REQUEST -- plane details

app.put("/api/admin/:pos/planes", (req, res) => {
    const {
        terminal,
        gate,
        airline_img,
        plane,
        destination,
        scheduled_time,
        statis
    } = req.body;

    const pos = [req.params.pos];

    if (!terminal || !gate || !airline_img || !plane || !destination || !scheduled_time || !statis) {
        return res.status(400).json({ error: "Invalid payload" });
    }
    pool.getConnection((error, connection) => {
        if (error) {
            return res.status(500).json({ error });
        }
        connection.beginTransaction(error => {
            if (error) {
                return res.status(500).json({ error });
            }
            connection.query(
                "UPDATE flight SET terminal=?,gate=?,airline_img=?,plane=?,destination=?,scheduled_time=?,statis=? WHERE planeid= ?",
                [terminal, gate, airline_img, plane, destination, scheduled_time, statis, pos],
                (error, results) => {
                    if (error) {
                        connection.rollback(() => {
                            return res.status(500).json({ error });
                        });
                    }
                    connection.commit(error => {
                        if (error) {
                            connection.rollback(() => {
                                return res.status(500).json({ error });
                            });
                        }
                        connection.release();
                        res.json(results.changedRows);
                    });
                });
        });
    });
});

//DELETE REQUEST -- plane details
app.delete("/api/admin/planes/:id", (req, res) => {
    const close = [req.params.id];
    pool.getConnection((error, connection) => {
        if (error) {
            return res.status(500).json({ error });
        }
        connection.beginTransaction(error => {
            if (error) {
                return res.status(500).json({ error });
            }
            connection.query(
                "DELETE FROM flight WHERE planeid=?",
                [close],
                (error, results) => {
                    if (error) {
                        return connection.rollback(() => {
                            return res.status(500).json({ error });
                        });
                    }
                    connection.commit(error => {
                        if (error) {
                            return connection.rollback(() => {
                                return res.status(500).json({ error });
                            });

                        }
                        connection.release();
                        res.json(results.affectedRows);
                    });
                });
        });
    });
});

/**************************************************************************************************
 * ************************************************************************************************
 * ************************************ADMIN PAGE**************************************************
 * ************************************************************************************************
 * ************************************************************************************************
 */


app.listen(9000, () => {
    console.log("server live ..")

});
